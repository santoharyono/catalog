/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package com.mitrais.demo.catalog.controller;

import com.mitrais.demo.catalog.dao.ProductDao;
import com.mitrais.demo.catalog.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Santo Haryono Weli
 * @version $Id: ProductController.java, v 0.1 2018-11-23 14:51 Santo Haryono Weli Exp $$
 */

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    private ProductDao productDao;

    @GetMapping("/")
    public Page<Product> findAllProducts(Pageable page) {
        return productDao.findAll(page);
    }

    @GetMapping("/{id}")
    public Product findById(@PathVariable("id") Product product) {
        return product;
    }
}
