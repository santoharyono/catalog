/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package com.mitrais.demo.catalog.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.mitrais.demo.catalog.entity.Product;

/**
 * @author Santo Haryono Weli
 * @version $Id: ProductDao.java, v 0.1 2018-11-23 14:50 Santo Haryono Weli Exp $$
 */
public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}
