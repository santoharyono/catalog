drop table if exists product;

create table product (
    id varchar(36),
    code varchar(100) not null,
    nama varchar(100) not null,
    primary key (id),
    unique (code)
);